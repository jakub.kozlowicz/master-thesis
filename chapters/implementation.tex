\chapter{Implementation}

The proposed system comprises multiple mobile robots managed by a central
controller. The central controller handles inter-robot communication and
mission assignments, while the robots execute the assigned missions, perform
path planning, and follow designated paths. The robots utilized in this system
are TurtleBot~2 models~\cite{2012:TurtleBot2, 2011:TurtleBot:Specification}.
The implementation leverages the Robot Operating System~2~(ROS2)
framework~\cite{2022:ROS2:Macenski:ScienceRobotics}, which is an open-source
middleware that provides a structured communication layer above the robots’
host operating systems. ROS2 is designed to be flexible and scalable, catering
to a diverse range of robotic applications from low-level device control to
high-level artificial
intelligence~\cite{2009:ROS:Quigley:RobotOperatingSystem}. Each robot functions
as a node within the ROS network, communicating with the central controller via
ROS messages. The central controller is also implemented as a separate node in
the ROS network. The robots employ the ROS Navigation~2
Stack~\cite{2020:Macenski:Navigation2} for path planning and navigation.

% ------------------------------------------------------------------------------
\section{System Architecture}

The system architecture, illustrated in Figure~\ref{fig:system-architecture},
\begin{figure}[hptb]
  \centering
  \input{figures/implementation/system-architecture.tex}
  \caption{System architecture}%
  \label{fig:system-architecture}
\end{figure}
is based on a centralized control paradigm. The central controller (supervisor)
manages the operations of all robots in the system, communicating with them
over the ROS network. It receives mission assignments from the user and
delegates these missions to individual robots. Constructed as a ROS2 package,
the central controller comprises multiple nodes, each responsible for distinct
tasks such as path discretization, collision avoidance, deadlock avoidance, and
motion control. The central controller provides request-reply communication
services to the robots, including path discretization and real-time
decision-making for segment traversal.

Each robot navigator is also implemented as a ROS2 package. Robots act as nodes
in the ROS network, using ROS messages to communicate with the central
controller. They utilize the ROS Navigation 2 Stack for path planning and
execution. The robots’ low-level control systems handle movement and
path-following, while high-level control systems manage communication with the
central controller and navigation through segmented paths (e.g., entering a
segment or waiting for it to be free).

% ------------------------------------------------------------------------------
\section{Robot Movement}

\subsection{Path Planning}

Path planning is a critical aspect of robot movement, allowing the robot to
determine an optimal route from its starting position to its goal while
avoiding obstacles. The system uses the ROS2 Navigation Stack, specifically the
\texttt{nav2\_planner}~\cite{2023:Macenski:Survey} module, which provides
several grid-based path planning algorithms. These algorithms take into account
the robot’s kinematic and dynamic constraints and the environment’s layout. The
path planner generates a series of waypoints that the robot must follow to
reach its destination safely. The planner considers factors such as the robot’s
size, shape, and maximum velocity to ensure that the generated path is feasible
for the robot to traverse.

To properly plan a path, the robot must have a map of its environment. Robots
have the map of the environment that have been built using the SLAM algorithm
beforehand. The map is represented as a grid, with each cell indicating whether
it is occupied by an obstacle or free space. The path planner uses this map to
determine the optimal path for the robot to follow.

The path planning module consists of two main components: the global planner
and the local planner. The global planner generates a path for the robot to
follow from its starting point to the destination. This path is computed using
algorithms like A* or Dijkstra’s algorithm, which consider the robot’s
environment as a grid map where each cell can be either free or occupied. The
planner uses a costmap, which is a representation of the environment that
includes information about obstacles, to ensure the path is collision-free. The
local planner, or \texttt{nav2\_controller}~\cite{2023:Macenski:Survey},
refines the global path and handles dynamic obstacles. It uses algorithms like
Dynamic Window Approach (DWA) or Timed Elastic Band (TEB) to adjust the path in
real-time. The local planner ensures the robot can navigate around unforeseen
obstacles that were not present on the initial costmap used by the global
planner.

\subsection{Path Following}

Path following~\cite{2023:Macenski:RegulatedPurePursuit} is the process by
which the robot moves along the planned path. This involves converting the
planned path into motor commands that drive the robot’s wheels. The
\texttt{nav2\_controller}~\cite{2023:Macenski:Survey} module translates the
global path into a series of waypoints. The robot uses these waypoints to
ensure it stays on the correct path. Controllers like
Proportional-Integral-Derivative (PID) controllers or more advanced model
predictive controllers (MPC) are used to generate appropriate wheel velocities.
The robot constantly monitors its position using sensors such as LIDAR, IMU,
and wheel encoders. The localization module uses solely AMCL (Adaptive Monte
Carlo Localization) to estimate the robot’s position and orientation in the
map. This information is used to determine the robot’s deviation from the path
and make necessary corrections. Feedback mechanisms ensure the robot can
correct its course in real-time, maintaining a smooth trajectory towards its
goal.

% ------------------------------------------------------------------------------
\section{Motion Control}

The flow of the mission assignment is depicted in
Figure~\ref{fig:mission-flow}. The motion control is illustrated in
Figure~\ref{fig:motion-flow}.
\begin{figure}[hptb]
  \centering
  \input{figures/implementation/mission-control.tex}
  \caption{Mission assignment flow}%
  \label{fig:mission-flow}
\end{figure}
\begin{figure}[hptb]
  \centering
  \input{figures/implementation/motion-control.tex}
  \caption{Motion control flow}%
  \label{fig:motion-flow}
\end{figure}
The central controller receives mission assignments from the user. If the
assigned robot is registered with the central controller, the mission is
accepted, otherwise rejected. The central controller instructs the robot to
plan its path to the designated goal. Using the ROS Navigation 2 Stack, the
robot plans its path and sends it back to the central controller. If not all
robots have their missions assigned, the central controller stores the planned
path for subsequent division until all of them send theirs paths to the central
controller. This requirement is due to usage of the optimal segment
discretization. The path can be optimally divided only if all the paths are
known. Each path segment is uniquely identified and contains information about
required resources, such as grid map cells or virtual resources when segments
intersect. The supervisor then sends the divided paths to the robots. Each
robot sequentially navigates through the path segments, requesting permission
from the central controller to enter each one. The central controller checks if
the segment is free and safe, especially to avoid potential deadlocks, and
responds with either permission or rejection. If permission is denied, the
robot periodically requests again. Upon receiving permission, the robot enters
and traverses the segment, then requests the next one at the segment's end.
This process continues until the robot reaches its goal. The low-level segment
traversal communication is shown in
Figure~\ref{fig:segment-traversal-communication-no-wait} and
Figure~\ref{fig:segment-traversal-communication-wait}.
\begin{figure}[hptb]
  \centering
  \input{figures/implementation/segment-traversal-communication-no-wait.tex}
  \caption{Segment traversal communication with no stop}%
  \label{fig:segment-traversal-communication-no-wait}
\end{figure}
\begin{figure}[hptb]
  \centering
  \input{figures/implementation/segment-traversal-communication-wait.tex}
  \caption{Segment traversal communication with stop}%
  \label{fig:segment-traversal-communication-wait}
\end{figure}


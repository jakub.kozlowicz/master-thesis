\chapter{Multi-robot Systems}

\section{Introduction to Multi-robot Systems}

Multi-robot systems (MRS) are made up of multiple mobile or stationary
autonomous robots that collaborate in distributed way. These systems stand out
for their smooth cooperation, which allows them to complete more complicated
tasks than one robot could manage on its own. The allocation of tasks among
several robots greatly increases work completion times and resource utilization.
In addition, the inherent redundancy of MRS improves the fault tolerance and
reliability of the system. Multi-Robot systems navigate and carry out missions
in both static and dynamic surroundings, frequently in a chaotic and
unpredictable condition, by using their aggregate capabilities. This cooperative
nature allows adaptability in different operational environments, which makes
MRS particularly valuable in situations characterized by environmental
uncertainty or task complexity. In addition, MRS facilitates scalability. With
an increase in the number of robots, the system can solve more complex or more
complex tasks without increasing the proportion of complexity. The modularity of
the MRS allows you to easily integrate new robots into existing systems, whether
they are identical or varied. The synergy of MRS leads to robust solutions in
complex situations such as environmental monitoring, disaster management, and
large-scale industrial
processes~\cite{2020:ARControl:Feng:CollaborativeRoboticManipulation,
  2004:SMCB:Farinelli:MultirobotSystems,
  2018:Automatica:Zhou:RobustControlMultiRobotSystems}.

\section{Types of Multi-robot Systems}

Multi-robot systems are typically divided into two main types: homogeneous and
heterogeneous systems. These systems have been categorized according to the
robots that make up them and their similarities or differences in
capability~\cite{2019:ACMCS:Rizk:CooperativeHeterogeneousMultiRobotSystems}.

\subsection*{Homogeneous Systems}

Each robot in a homogeneous multi-robot system has the same hardware and
software features. Since all robots have the same capabilities and limitations,
plan, control, and coordination strategies are disentangled, empowering
sending and organizing to be more proficient. The most points of interest of a
homogeneous framework are that they are simple to operate and keep up since they
can utilize the same components and program upgrades anywhere. When employments
have great consistency and repetition, such as computerized stockrooms or
gathering lines, homogeneous frameworks work well.

However, the flexibility and adaptability of the uniform system are limited.
Since all robots are the same, the system may not be able to handle tasks that
require various skills. For example, in conditions such as search and rescue
missions, a homogeneous system may not function as well as a heterogeneous
system. It does not have specialized robots to perform multiple tasks
simultaneously, such as debris removal and overhead monitoring. Despite these
disadvantages, homogeneous systems are a reasonable option for some applications
due to their low cost and simple
implementation~\cite{2000:Stone:Veloso:MultiagentSystems,
  2004:SMCB:Farinelli:MultirobotSystems}.

\subsection*{Heterogeneous Systems}

Heterogeneous multi-robot systems include robots that are adapted to perform
specific tasks in a group and have different capabilities. Due to their
differences, team robots are better equipped to deal with a wider variety of
tasks since they can individually play their strengths in challenging and
changing environments.For illustration, a heterogeneous framework might comprise
of ground robots for physical or transport operations and discuss rambles for
surveillance, each of which would move forward the capabilities of the other.

The challenge with heterogeneous systems is to make it more difficult to
coordinate and facilitate various kinds of robot. In order to guarantee the
successful cooperation and smooth communication between the various robots,
advanced algorithms and cutting-edge communication protocols are required. These
systems often use a distributed control topology, where each robot contributes
to the overall goal of the mission, making decisions based on local input. The
improved resilience and adaptability of the system allow the plan to deal with
unexpected environmental changes and dynamic
conditions~\cite{2000:Stone:Veloso:MultiagentSystems,
  2014:ACC:Twu:HeterogeneityMultiAgentSystems}.

\subsection*{Comparison}

Both systems have been broadly considered and created, with applications
extending from look and protect operations to industrial automation. Homogeneous
systems tend to win in situations where tasks are repeated and
well-characterized, such as in manufacturing. The consistency of these systems
allows for effective large-scale arrangements with the least complexity in
control and coordination.

On the other hand, heterogeneous systems are often preferred in dynamic
scenarios such as disaster response, where adaptation and specialty are crucial.
The various capabilities of heterogeneous robots allow them to tackle various
tasks simultaneously, thus improving the overall effectiveness of the mission.

The choice between homogeneous and heterogeneous systems depends on the specific
requirements and limitations of the application. Although homogeneous systems
offer simplicity and cost-effectiveness, heterogeneous systems offer flexibility
and adaptability to more complex and unpredictable environments. Progress in
robotics and artificial intelligence continues to blur the boundaries between
these categories and enables the development of hybrid systems that combine the
best features of the two~\cite{2023:cs.RO:Bettini:HeterogeneousMultiRobotRL}.

\section{Operating Environments}

Multi-Robot systems operate in a variety of environments, including structured
and unstructured settings, each presenting unique challenges and opportunities.
The choice of environment significantly impacts the design, control and
performance of multi-robot systems, as different tasks and missions require
specific capabilities and adaptations~\cite{2012:MS:Bruzzone:LocomotionSystems}.

\subsection*{Structured Environments}

Structured environments, such as manufacturing floors, are characterized by
their defined and predictable nature. These environments are typically
well-mapped and have fixed pathways and stations, allowing robots to follow
predetermined routes and schedules. The controlled nature of these environments
minimizes uncertainties and obstacles, making task execution more efficient.

In structured environments, multi-robot systems can rely heavily on centralized
control systems and precise coordination. For example, in an automated
warehouse, robots can efficiently manage inventory by following fixed paths to
retrieve and store items, reducing human labor and increasing operational speed.
The predictability of these settings also simplifies the deployment of
homogeneous robot teams, which can be easily managed and maintained due to their
uniformity.

Moreover, structured environments benefit from advanced scheduling algorithms
that optimize workflow, ensuring that robots perform their tasks in the most
efficient sequence and timing. This leads to increased productivity and reduced
operational costs. Robotics in structured settings often integrates seamlessly
with existing industrial infrastructure, facilitating a smooth transition to
automation. However, the rigidity of these environments can limit the
flexibility of robots to handle unexpected changes or new tasks without
significant reprogramming~\cite{2000:Murphy:DisasterRobotics,
  2023:AppliedSciences:Wijayathunga:ChallengesSolutionsAutonomousGroundRobot}.

\subsection*{Unstructured environments}

Unstructured environments, such as disaster sites or extraterrestrial surfaces,
are characterized by unpredictability and hazards. In these settings, robots
must navigate uneven terrain, debris, and unknown obstacles, often requiring
real-time decision-making and adaptability. Advanced sensing and autonomous
navigation technologies are critical for success in these challenging
conditions.

In unstructured environments, the reliance on heterogeneous multi-robot systems
becomes more pronounced. Each robot can be equipped with specialized sensors and
tools to meet specific challenges. The collaborative effort of diverse robots
enhances the overall success of the mission, leveraging the unique capabilities
of each type.

Autonomous navigation in unstructured environments requires sophisticated
algorithms for path planning, obstacle avoidance, and environment mapping.
Robots must be capable of interpreting sensor data on the fly and adjusting
their actions accordingly. Machine learning techniques are often used to improve
robots' ability to recognize and respond to new situations. Furthermore, robust
communication networks are essential to ensure seamless information exchange
between robots and their control centers, particularly in remote or hazardous
locations where human intervention is limited.

The flexibility of multi-robot systems in unstructured environments allows them
to adapt to rapidly changing conditions, making them indispensable in scenarios
where human presence is risky or impossible. However, the complexity of these
environments requires extensive testing and validation to ensure the reliability
and safety of robotic systems. Innovations in materials science, energy storage,
and artificial intelligence continue to push the boundaries of what multi-robot
systems can achieve in these demanding
settings~\cite{2010:Katz:HowRobotsSucceedUnstructuredEnvironments,
  2000:Murphy:DisasterRobotics,
  2023:AppliedSciences:Wijayathunga:ChallengesSolutionsAutonomousGroundRobot}.

\section{Features and Challenges of Multi-robot Systems}

\subsection*{Coordination}

Effective task allocation and synchronization between robots are crucial for
efficiency and effectiveness. Algorithms must be developed to ensure that robots
can dynamically assign tasks among themselves and reallocate resources as needed
to handle changing environments and mission requirements. Coordination
mechanisms frequently incorporate a combination of centralized and decentralized
approaches. Centralized coordination can optimize global goals, but may suffer
from single point of failure and scalability issues. In contrast, decentralized
coordination improves robustness and scalability but requires sophisticated
local decision-making and communication protocols to ensure coherent group
behavior~\cite{2004:Gerkey:Matarić:TaskAllocationIJRR}.

Decentralized coordination can involve behavior-based approaches inspired by
natural phenomena, such as flocking, where robots follow simple rules to
maintain formation and avoid collisions. For example, Reynolds' flocking rules,
namely flock centering, obstacle avoidance, and velocity matching, have been
adapted for robotic applications, allowing groups of robots to move cohesively
while avoiding obstacles and dynamically adjusting their
paths~\cite{2012:ICIIS:Gautam:MultiRobotSystems}. Furthermore, hybrid approaches
that combine centralized and decentralized strategies can leverage the benefits
of both, providing a robust framework for multi-robot
coordination~\cite{2015:CCAA:Doriya:MultiRobotCommunicationCoordination}.

\subsection*{Communication}

Reliability in dynamic or hostile environments is essential for coordination.
This involves developing robust communication protocols that can handle
interference, bandwidth limitations, and the need for secure data transmission.
Multi-hop communication, where messages are relayed through intermediate robots
to extend the communication range, and the use of communication relays is often
employed to maintain connectivity. Furthermore, adaptive communication
strategies that adjust transmission power and frequency based on environmental
conditions can enhance robustness. Ensuring low-latency and high-throughput
communication is critical, especially in time-sensitive applications like search
and rescue operations.

Several approaches have been proposed to address the communication challenges in
multi-robot systems. One such approach is the use of ad hoc mobile networks,
which facilitate flexible and dynamic communication among robots. These networks
employ various routing protocols to effectively manage data transmission. For
example, the Cluster Head Gateway Switch Routing (CGSR) protocol organizes
robots into clusters, with a cluster head responsible for communication within
the cluster and with other clusters. This hierarchical approach helps manage the
communication load and extend the reach of the
network~\cite{2015:CCAA:Doriya:MultiRobotCommunicationCoordination}.

Moreover, the integration of cloud computing with multi-robot systems offers new
avenues for enhancing communication and coordination. Cloud-enabled
architectures allow robots to offload computationally intensive tasks to remote
servers, reducing the on-board processing load and improving overall system
efficiency. This setup also facilitates real-time data sharing and collaborative
decision-making among robots, using the computational power and storage
capabilities of the
cloud~\cite{2015:CCAA:Doriya:MultiRobotCommunicationCoordination}.

\subsection*{Scalability}

Scalability in multi-robot systems is essential for accommodating increasing
numbers of robots without significant performance degradation. Scalability in
multi-robot systems is largely influenced by the design of the algorithms used
for coordination and task allocation. Algorithms should be capable of
maintaining performance as the number of robots increases. For example, the
study by Portugal and Rocha evaluated five different patrolling algorithms
(Conscientious Reactive, Heuristic Conscientious Reactive, Heuristic Pathfinder
Conscientious Cognitive, Cyclic Algorithm for Generic Graphs and Multilevel
Subgraph Patrolling) in various environments to determine their scalability and
performance. The study found that offline planning strategies such as MSP
(Multilevel Subgraph Patrolling) and CGG (Cyclic Algorithm for Generic Graphs)
generally perform better in weakly connected environments with larger teams,
whereas reactive algorithms such as CR (Conscientious Reactive) and HCR
(Heuristic Conscientious Reactive) perform better in strongly connected
environments with smaller
teams~\cite{2013:AR:Portugal:MultiRobotPatrollingAlgorithms}.

Scalability also involves ensuring that all tasks can be scheduled and
completed within their deadlines, even as the number of robots increases.
Real-time schedulability analysis is essential for this
purpose.~\cite{2003:ICRA:Sweeney:ScalabilitySchedulabilityMultiRobotSystems}~propose
a heuristic algorithm for real-time schedulability analysis that takes into
account communication costs and processor workloads. This approach helps to
predict the scalability of the system and design task models that can
accommodate additional robots without compromising
performance~\cite{2013:AR:Portugal:MultiRobotPatrollingAlgorithms}.

\subsection*{Reliability}

Ensuring reliability in multi-robot systems also involves developing robust
control algorithms. According to a study
by~\cite{2018:Automatica:Zhou:RobustControlMultiRobotSystems}, a distributed
approach to robust control can minimize the impact of individual robot failures
on the overall system. The robust control algorithms proposed ensure that the
failure of a robot affects only those directly interacting with it, preventing a
cascade of failures throughout the system. This is achieved by dividing robots
into reliable and unreliable categories and using distributed control strategies
to manage their interactions and mitigate the effects of
failures~\cite{2018:Automatica:Zhou:RobustControlMultiRobotSystems}.

Another important aspect of reliability is the detection and recovery from
failures. Implementing real-time health monitoring and diagnostic systems can
help identify potential failures before they occur. Techniques such as
redundancy, where multiple robots perform the same task to ensure that failure
of one does not compromise the mission, and fault-tolerant designs that allow
robots to detect and recover from failures autonomously, are critical to
maintaining reliability in multi-robot
systems~\cite{2018:Automatica:Zhou:RobustControlMultiRobotSystems}.

In addition, task reallocation strategies play a vital role in maintaining
reliability. When a robot fails, the system must be able to dynamically
reallocate its tasks to other robots to ensure mission continuity. This requires
sophisticated algorithms that can quickly and efficiently redistribute tasks
without causing significant delays or disruptions. The use of machine learning
and artificial intelligence can further enhance the reliability of task
reallocation by allowing the system to learn from past failures and improve its
decision-making processes over
time~\cite{2018:Automatica:Zhou:RobustControlMultiRobotSystems}.

In addition, robust communication protocols are essential for reliability. In
environments where communication can be intermittent or unreliable, ensuring
that robots can still coordinate effectively is crucial. Techniques such as
multi-hop communication, where messages are relayed through multiple robots to
extend the communication range, and adaptive communication strategies that
adjust parameters based on current conditions, can help maintain reliable
communication links between robots, even in challenging
environments~\cite{2018:Automatica:Zhou:RobustControlMultiRobotSystems}.

\subsection*{Collision and Deadlock Avoidance}

Robots must be able to detect and avoid collisions with each other and with
obstacles in real-time. This requires sophisticated sensing systems, such as
LiDAR, radar, and computer vision, and real-time processing capabilities to
interpret sensor data and make quick decisions. Predictive modeling, where
robots anticipate the future positions of obstacles and other robots, and
machine learning techniques, which allow robots to learn and improve their
collision avoidance strategies from experience, are often employed to enhance
performance.

Recent approaches to collision avoidance in multi-robot systems have emphasized
the use of distributed
algorithms.~\cite{2018:Automatica:Zhou:RobustControlMultiRobotSystems} propose a
distributed real-time algorithm for the avoidance of collisions and deadlock. In
this method, each robot autonomously checks its succeeding states and stops if a
collision or deadlock is predicted. This algorithm ensures that no more than one
robot occupies a given collision zone at a time, thereby preventing collisions
and improving the overall performance of the
system~\cite{2017:IEEE:Zhou:CollisionDeadlockAvoidanceMultiRobotSystems}.

The algorithm models the motion of each robot as a labeled transition system,
where each robot can autonomously execute the algorithm to avoid collisions and
deadlocks. When a robot detects that its next move could lead to a collision or
a deadlock, it stops and waits for the other robot to move first. This
decision-making process is managed locally by each robot, allowing the system to
be scalable and
robust~\cite{2017:IEEE:Zhou:CollisionDeadlockAvoidanceMultiRobotSystems}.

In addition to avoiding collisions, the algorithm addresses deadlocks by
introducing negotiation strategies. When multiple robots need to move to the
same location, they negotiate to determine which robot moves first, thus
avoiding decision deadlocks and improving the efficiency of the system. If a
deadlock is detected, the robots involved negotiate to resolve the deadlock,
ensuring that at least one robot can move and thus breaking the deadlock
cycle~\cite{2017:IEEE:Zhou:CollisionDeadlockAvoidanceMultiRobotSystems}.

The distributed approach also incorporates advanced collision avoidance
strategies that combine high-level discrete control with low-level continuous
feedback control. This dual-layered control mechanism allows for more effective
planning and execution of collision avoidance maneuvers, providing a
comprehensive solution that seamlessly integrates both
phases~\cite{2017:IEEE:Zhou:CollisionDeadlockAvoidanceMultiRobotSystems}.

Ensuring safe and efficient navigation in dynamic environments is a critical
aspect of multi-robot system design. Continuous advancements in sensor
technology and computational algorithms are essential to maintaining high
performance and reliability in avoiding collisions and deadlocks. Using these
advancements, multi-robot systems can achieve greater autonomy and robustness in
various applications, from search and rescue missions to industrial
automation~\cite{2017:IEEE:Zhou:CollisionDeadlockAvoidanceMultiRobotSystems}.

\section{Multi-mobile Robot Systems}

Multi-mobile robot systems (MMRS) refer to a subclass of multi-robot systems
characterized by the mobility of the robots involved. These systems consist of
multiple autonomous mobile robots that collaborate to perform tasks in various
environments, representing a highly adaptive and scalable approach to the
execution of complex tasks. MMRS leverages advanced coordination algorithms to
manage dynamic task allocation and real-time synchronization, crucial for
navigating unpredictable environments. Robust communication protocols ensure
seamless information exchange, allowing coordinated efforts among robots.
Scalability is maintained through algorithms designed to handle an increasing
number of robots without significant performance degradation, utilizing
decentralized control and real-time schedulability analysis. Reliability is
achieved through fault-tolerant designs, redundancy, real-time health
monitoring, and diagnostic systems, which detect and address potential failures
autonomously. The integration of machine learning and AI allows MMRS to learn
from past experiences, continually improving reliability and performance. These
systems exemplify the potential of collaborative robotics in improving
efficiency, adaptability, and robustness, effectively tackling applications
ranging from industrial automation to disaster response and environmental
monitoring~\cite{2012:ICIIS:Gautam:MultiRobotSystems,
  2017:IEEE:Zhou:CollisionDeadlockAvoidanceMultiRobotSystems}.


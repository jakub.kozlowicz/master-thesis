\chapter{Robot Control}

\section{Introduction to Robot Control}

Robot control is a crucial aspect of modern robotics, focusing on the design and
implementation of controllers to achieve desired behaviors in robotic systems.
Initially, the field of robotics was limited by high computational costs,
inadequate sensors, and a fundamental lack of understanding of control systems,
restricting applications to simple tasks like material handling and welding.
However, advances in technology and control theory have expanded the
capabilities of robotic systems, enabling them to perform complex tasks such as
planetary exploration and autonomous navigation. A robot controller, a vital
component, defines the accuracy and repeatability of a robot by modifying its
behavior through computational algorithms and actuation mechanisms. Effective
control systems can be classified into open-loop and closed-loop types, with the
latter incorporating feedback to improve stability and performance. As robotics
continues to evolve, the integration of more intelligent control strategies,
such as supervised control for multi-mobile robot systems and discrete control
methods, becomes essential to meet the increasing complexity and demands of
modern applications. The complexity of these systems arises from the need to
manage and coordinate multiple robots simultaneously, each performing different
tasks while avoiding collisions and ensuring efficient
operation~\cite{2015:Badoni:RoboticsController}.

\section{Types of Control Architectures}

Control architectures define the structure and organization of the control
system and determine how robots interact with each other and the environment.
These architectures can be classified into several categories, each with its
advantages and disadvantages.

\textbf{Centralized control} involves a single supervisor who oversees the
entire system, makes decisions, and coordinates robot activities. This approach
is simple and efficient for small systems, but it can become a bottleneck as the
system scales, leading to increased computational complexity and communication
overhead. Centralized control architectures are suitable for applications that
require tight coordination and synchronization between robots, such as formation
control and cooperative
manipulation~\cite{2021:Hernando:Alonso:BehaviorBasedControlArchitecture,
  2021:Roszkowska:Jakubiak:ControlSynthesis, 2023:Roszkowska:Janiec:DEDS}. The
central supervisor’s ability to have a global view of the system allows optimal
decision-making, but this central point of failure can significantly affect the
entire system if it fails.

\textbf{Decentralized control} distributes decision making among robots,
allowing them to coordinate their actions independently based on local
information. This approach is more scalable and robust than centralized control,
but can lead to conflicts and inefficiencies if robots do not communicate
effectively~\cite{2021:Hernando:Alonso:BehaviorBasedControlArchitecture,
  2021:Roszkowska:Jakubiak:ControlSynthesis, 2023:Roszkowska:Janiec:DEDS}.
Decentralized control architectures are suitable for applications that require
flexibility and adaptability, such as search and rescue missions and
surveillance. The lack of a single point of failure enhances system robustness,
but achieving global system objectives can be challenging without effective
inter-robot communication.

\textbf{Distributed control} combines elements of centralized and decentralized
control, with multiple supervisors working together to manage different aspects
of the system. This approach is flexible and scalable, making it suitable for
large, complex systems with diverse requirements. Distributed control
architectures are suitable for applications that require fault tolerance and
redundancy, such as multi-robot exploration and
mapping~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis,
  2023:Roszkowska:Janiec:DEDS}. Using multiple supervisors, the system can
balance the benefits of both centralized decision-making and decentralized
autonomy, thus improving overall system performance and reliability.

\textbf{Hierarchical control} architectures organize the control system into
multiple levels of abstraction, each level responsible for a different aspect of
the system. This approach enables supervisors to manage the system at multiple
levels, ensuring that the robots work seamlessly to achieve their
objectives~\cite{2021:Hernando:Alonso:BehaviorBasedControlArchitecture,
  2023:Roszkowska:Janiec:DEDS}. Hierarchical control architectures are suitable
for applications that require complex coordination and cooperation between
robots, such as multi-robot manipulation and
assembly~\cite{2021:Hernando:Alonso:BehaviorBasedControlArchitecture,
  2023:Roszkowska:Janiec:DEDS}. By hierarchically structuring the control
system, it is possible to manage detailed local interactions at lower levels
while guiding the overall behavior of the system at higher levels, thus
optimizing both local and global performance.

By selecting the appropriate control architecture for a given application,
supervisors can ensure that the robots work together effectively to achieve
their objectives. This decision is influenced by factors such as the size and
complexity of the system, the level of coordination required, and the
availability of communication and computational resources. Using the advantages
of different control architectures, supervisors can design robust, efficient, and
scalable control systems that enable robots to perform complex tasks in a wide
range of applications.

\section{Supervisory Control in Multi-robot Systems}

Supervisory control in multi-robot systems involves overseeing and directing
robot activities at a high level, while robots handle low-level execution
autonomously. This approach ensures that robots can perform complex tasks
efficiently and safely. Supervisory control in multi-robot systems includes
mission planning, task allocation, and monitoring. The supervisor sets the
overall mission objectives and constraints, while algorithms help break these
objectives down into specific tasks that robots can execute. The supervisor
also monitors the system’s performance and intervenes when necessary to adjust
strategies or respond to unexpected situations. A multilevel hierarchical
control system is commonly used in multi-robot systems, consisting of three
levels of control: the top level being a supervisor based on a discrete
representation of the Multiple Mobile Robot System (MMRS), an intermediate
level supervising the execution of robot motion on individual stages, and the
lowest level responsible for actual motion execution. The top level supervisor
ensures collision- and deadlock-free movement by centrally controlling changes
in robot stages, viewing robot motion processes as sequences of
stages~\cite{2023:Roszkowska:Janiec:DEDS}.

\subsection{Hybrid Control Systems}

Hybrid control systems integrate discrete event systems (DES) and
continuous-time systems (CTS) to leverage the strengths of both. DESs are adept
at handling discrete event-driven behaviors, such as task initiation and
completion. They are used to model discrete and logical aspects of robotic
systems, such as task transitions, synchronization, and decision points. They
provide a structured way to handle sequences of events and state changes,
essential for coordinating complex multi-robot tasks. On the other hand, CTS
manages continuous dynamic behaviors such as trajectory tracking, velocity
control, dynamic response, and speed control. They use differential equations
and control theory to model and control the physical movements of robots,
ensuring smooth and precise execution of
tasks~\cite{2023:Roszkowska:Janiec:DEDS}.

To ensure efficient and correct operation of the MMRS, the supervisory control
uses a DES model to represent concurrent robot movements along specified paths.
This model employs the Petri net formalism to ensure the required logic of MMRS
operations, such as avoiding collision and deadlock, through a set of control
procedures that adjust robot motions according to supervisor decisions. The
low-level robot control, based on CTS, ensures that the robots follow the
desired paths and velocity profiles
accurately~\cite{2023:Roszkowska:Janiec:DEDS}.

By combining these approaches, hybrid systems can provide a comprehensive
framework for robot control, allowing precise motion execution alongside robust
event handling. This modular construction of the hybrid control system also
facilitates modifications and experimental examinations to optimize MMRS
performance under various operational
conditions~\cite{2023:Roszkowska:Janiec:DEDS}.

\subsection{Supervisory Control Models}

Supervisory control models for multi-mobile robot systems utilize discrete event
systems (DES) to manage the coordination and control of multiple robots. The
primary model used in this context is the Petri net formalism, which provides a
robust framework for representing concurrent processes and their interactions.
Petri nets are particularly useful for modeling the synchronization and conflict
resolution required in multi-robot systems. They allow the representation of
complex dependencies and constraints, ensuring that robots operate without
collisions and deadlocks~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis}.

A typical Petri net model for MMRS includes places representing different states
or locations of robots, transitions that indicate state changes or movements,
and tokens that mark the current state of the system. The control logic is
embedded in the structure of the Petri net, dictating the permissible sequences
of actions based on the system's current state. This method ensures that all
robots can complete their tasks efficiently while complying with the necessary
safety and operational constraints~\cite{2010:Cassandras:DiscEventSystems}.

Another widely used model for supervisory control in multi-robot systems is the
Finite State Machine (FSM). FSMs are computational models that represent the
system's states and the transitions between these states. In the context of
multi-mobile robot systems, FSMs can be employed to model and control the
behaviors of individual robots as well as their interactions. Each state in an
FSM represents a specific behavior or mode of operation of a robot, and
transitions between states are triggered by events or conditions in the
environment~\cite{2010:Cassandras:DiscEventSystems}.

FSMs are particularly advantageous because of their simplicity and ease of
implementation. They provide a clear and intuitive way to design control logic
by defining explicit states and transitions. This makes it easier to debug and
verify the system's behavior. Moreover, FSMs can be extended to Hierarchical
Finite State Machines (HFSMs), which introduce hierarchy into the state machine,
allowing more complex behaviors to be managed in a modular and scalable
manner~\cite{2010:Cassandras:DiscEventSystems}.

Supervisory control models are essential for managing the interactions between
robots in multi-robot systems, ensuring they work together effectively to
achieve common goals. These models can be classified into centralized,
decentralized, and distributed control architectures, each with its unique
advantages and disadvantages. Furthermore, supervisory control models vary on
the basis of the level of abstraction they provide. High-level models focus on
mission planning and task allocation, whereas low-level models focus on motion
planning and execution. By integrating these models, the supervisors can manage
the system at multiple levels, ensuring that the robots work seamlessly together
to achieve their objectives~\cite{2023:Roszkowska:Janiec:DEDS}.

\subsection{Algorithms for Supervisory Control}

Supervisory control algorithms play a crucial role in managing multi-robot
systems, ensuring that the robots can work together effectively to achieve their
objectives. These algorithms can be classified into several categories, each
with its strengths and weaknesses. The key algorithms used in supervisory
control of multi-robot systems include task allocation algorithms, path planning
algorithms, and decision-making algorithms.

\subsubsection{Task Allocation Algorithms}

Multi-robot Task Allocation (MRTA) is a complex problem within the field of
multi-robot systems (MRS) that involves optimally assigning a set of robots to a
set of tasks to maximize overall system performance under various constraints.
This problem is particularly challenging when dealing with heterogeneous robots
equipped with different capabilities and required to perform tasks with diverse
requirements and constraints. There are two primary approaches to solving the
MRTA problem: metaheuristic-based and market-based approaches.
Metaheuristic-based approaches, such as genetic algorithms and ant colony
optimization, utilize population-based algorithms that leverage multiple agents
to search for an optimal solution. These approaches have been shown to be
effective in handling complex, heavily constrained applications that involve
numerous heterogeneous tasks and robots. For example, genetic algorithms have
been applied to group tracking systems and time-extended task allocation
scenarios, while ant colony optimization has been used to solve cooperation
tasks among multiple robots~\cite{2015:Khamis:TaskAllocationReviewStateofArt}.
Market-based approaches, on the other hand, involve the allocation of tasks
through bidding mechanisms, where robots bid for tasks based on their
capabilities and costs. This method has proven effective in scenarios where task
requirements and robot capabilities need to be matched dynamically. Studies have
shown that while metaheuristic approaches often outperform market-based
approaches in scalability, both methods are comparable in handling real-world
constraints such as time and robot capability-task requirement
matching~\cite{2015:Khamis:TaskAllocationReviewStateofArt}.

The MRTA problem can be categorized into different schemes and organizational
paradigms. Schemes include single-task (ST) versus multi-task (MT), and
single-robot (SR) versus multi-robot (MR), indicating whether robots perform
tasks sequentially or simultaneously and whether tasks require one or multiple
robots. Organizational paradigms can be centralized, where a central agent
allocates tasks, or decentralized, where each robot makes task allocation
decisions independently. Centralized approaches, while effective for small-scale
and static environments, face scalability issues and potential single points of
failure. Decentralized approaches, such as consensus-based auction algorithms
and hierarchical market-based methods, offer robustness and flexibility, making
them suitable for dynamic and large-scale
environments~\cite{2015:Khamis:TaskAllocationReviewStateofArt}.

\subsubsection{Path Planning Algorithms}

Path planning for multi-mobile robot systems involves determining optimal and
collision-free paths in dynamic environments, and it is integral to supervisory
control strategies. The hierarchical approach to path planning includes three
levels: the Supervisory Global Planner (SGP), the Global Planner (GP), and the
Local Planner (LP). The SGP operates on a static map, designating waypoints that
the GP uses to generate a global path while considering both static and
dynamically detected obstacles. The LP further refines this path in real-time to
avoid unforeseen obstacles. Various path planning algorithms play a crucial role
in this process. Graph-based algorithms such as A*, Dijkstra’s, and D* are
foundational, with A* being particularly noted for its efficiency in balancing
cost and distance to the target~\cite{2019:ETFA:Indri:GlobalPathPlanning}.
Sampling-based algorithms, including Rapidly-exploring Random Trees (RRT) and
Probabilistic Roadmaps (PRM), provide solutions for complex, high-dimensional
spaces by randomly sampling feasible
paths~\cite{2019:ETFA:Indri:GlobalPathPlanning}. Optimization-based methods like
Mixed-Integer Linear Programming (MILP) and Sequential Quadratic Programming
(SQP) offer precise, constraint-based solutions, but are computationally
intensive~\cite{2019:ETFA:Indri:GlobalPathPlanning}. The hierarchical
supervisory control system ensures that the global path integrates a safe
trajectory, continuously updated by the local planner to navigate around dynamic
obstacles effectively. This approach has been experimentally validated in
scenarios such as industrial environments, where robots must navigate safely
among humans and machinery~\cite{2019:ETFA:Indri:GlobalPathPlanning}.

\subsubsection{Real-time Decision-making Algorithms}

Decision-making algorithms in multi-agent systems (MAS) are critical for
enabling agents to accomplish complex tasks cooperatively. These algorithms
range from Markov decision processes (MDP), game theory, swarm intelligence, to
graph-theoretic models, each with its strengths and applications. MDPs are
widely used for modeling decision making where the outcomes are stochastic and
depend on the current state and action taken, satisfying the Markov property
where the future state depends only on the current state and action. This
approach is extended to partially observable environments using POMDPs, which
account for uncertainties and incomplete information, making them suitable for
dynamic and unpredictable settings~\cite{2018:Razik:DecisionMakingSurvery}. Game
theory, traditionally used for competitive environments, has found applications
in cooperative MAS by enabling agents to model and predict the actions of
others, thus optimizing collective outcomes. Bayesian games, a subset of game
theory, handle uncertainties in agents' rewards, crucial for strategic
decision-making in environments with incomplete
information~\cite{2018:Razik:DecisionMakingSurvery}. Swarm intelligence,
inspired by social animals, employs simple rules and local interactions among
homogeneous agents to achieve global objectives, making it robust and scalable
for tasks such as foraging and path
planning~\cite{2018:Razik:DecisionMakingSurvery}. Graph-theoretic approaches,
such as influence diagrams (IDs), provide a structured framework for
decision-making by incorporating actions and utilities into Bayesian networks,
allowing the representation of complex dependencies and sequential decision
making~\cite{2018:Razik:DecisionMakingSurvery}. These algorithms must also
consider factors such as system heterogeneity, computational efficiency, and
adaptability to dynamic environments to be effective in real-world applications,
including robotics, intelligent transport systems, and smart grids. Despite
significant advancements, challenges remain to develop scalable, autonomous, and
efficient algorithms that take advantage of the advances of big data and the
Internet of Things (IoT) to handle increasingly complex tasks in
MAS~\cite{2018:Razik:DecisionMakingSurvery}.

\section{Low-level Robot Control}

Low-level robot control involves the execution of specific movements and actions
as dictated by higher-level supervisory control. Objectivities are to ensure
that robots follow planned routes accurately, maintain desired speeds, and
respond dynamically to changes in their environment.

\subsection{Path-following Algorithms}

In the context of low-level robot control, path-following algorithms play a
crucial role in ensuring that each robot adheres to a predefined path with
minimal deviation. The main goal of these algorithms is to minimize the distance
between the robot's current position and its desired path while also controlling
the orientation and velocity to achieve smooth motion. Path-following techniques
are essential for the effective operation of multiple mobile robot systems
(MMRS), particularly in scenarios where precise navigation is required to avoid
obstacles and ensure mission success.

Commonly used path-following algorithms include pure pursuit, which involves
calculating a target point along the desired path at a fixed look-ahead distance
and steering the robot towards this point. This method is simple and effective
for smooth paths, but it can be difficult to maneuver with sharp turns and
high-speed maneuvers~\cite{1992:Coulter:PurePursuitPathTracking}. Another widely
adopted method is the Stanley controller, which focuses on minimizing the
lateral error and heading error to maintain the robot's alignment with the path.
This approach is particularly effective for autonomous vehicles operating at
various speeds and has been successfully implemented on different robotic
platforms~\cite{2006:Thrun:Stanley}.

Advanced path following techniques often incorporate model predictive control
(MPC), which uses a dynamic model of the robot to predict its future states and
optimize the control inputs over a finite time horizon. This method allows for a
more sophisticated handling of constraints and dynamic environments, making it
suitable for complex scenarios with moving
obstacles~\cite{2003:Qin:Badgwell:SurveyMPC}. Additionally, sliding mode control
offers robustness against disturbances and model uncertainties by switching
between different control laws based on the robot's state, ensuring reliable
path tracking even in challenging
conditions~\cite{1992:Utkin:SlidingModesControlOptimization}.

The integration of these algorithms within a hybrid control system, which
combines discrete event supervision control with continuous-time motion control,
improves the overall performance of MMRS.\@The supervisory level handles
high-level task allocation and coordination, ensuring collision avoidance and
mission completion, while the low-level control algorithms manage the real-time
execution of path following
tasks~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis}. This hierarchical
approach allows for efficient and scalable control of multiple robots, allowing
them to operate concurrently in shared workspaces without interfering with each
other's paths~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis}.

\subsection{Motion Control Procedures}

Motion control algorithms for mobile robots are a critical aspect of low-level
robot control, ensuring the precise and reliable execution of movements in
dynamic environments. These algorithms typically integrate various control
mechanisms to achieve accurate path follow and obstacle avoidance. One widely
used approach is differential drive control, which calculates the velocities of
the left and right wheels of the robot to achieve the desired trajectory and
orientation~\cite{2000:RoboCup:MotionControlDynamicMultiRobotEnvironments}. This
method involves reactive control equations that adjust wheel velocities based on
the robot’s current position and the target configuration, effectively handling
both stationary and moving targets through continuous replanning and feedback
adjustment~\cite{2000:RoboCup:MotionControlDynamicMultiRobotEnvironments}.

Advanced implementations of these algorithms often incorporate predictive
models, such as Kalman filters, to estimate the positions of dynamic obstacles
and adjust the robot’s path accordingly. The use of such models improves the
robot's ability to navigate complex environments, as demonstrated in robotic
soccer scenarios, where robots must maneuver around opponents and intercept a
moving ball~\cite{2000:RoboCup:MotionControlDynamicMultiRobotEnvironments,
  2021:Roszkowska:Jakubiak:ControlSynthesis}. Additionally, path-following
algorithms like pure pursuit and its spline-based modifications are employed to
minimize tracking errors and ensure smooth motion along predefined paths. These
algorithms calculate the curvature needed to follow the path accurately,
adjusting the robot’s velocity and orientation to reduce
deviations~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis}.

In summary, motion control algorithms for mobile robots combine differential
drive control, predictive modeling, and advanced path-following techniques to
achieve robust and precise navigation in dynamic environments, ensuring
effective low-level control essential for various
applications~\cite{2000:RoboCup:MotionControlDynamicMultiRobotEnvironments,
  2021:Roszkowska:Jakubiak:ControlSynthesis, 2023:Roszkowska:Janiec:DEDS}.


\chapter{Summary}

This study addresses the development of models and algorithms for the
supervisory control of multiple mobile robot systems (MMRS), with an emphasis
on ensuring safe navigation and effective coordination among robots. The
primary objective is to improve the operational efficiency and reliability of
MMRS in static environments by employing sophisticated control strategies and
algorithms.

The research introduces a hierarchical control framework comprising three
levels: high-level supervisory control, mid-level stage traverse control, and
low-level robot motion control. This framework integrates discrete event
systems (DES) and continuous-time systems (CTS), enabling a comprehensive
approach to managing the complex tasks and movements of multiple robots. The
high-level control focuses on mission planning and task allocation, while
mid-level control oversees the transitions between discrete path segments and
continuous motion. Low-level control is responsible for the precise execution
of robot movements, using continuous feedback to ensure accurate path
follow-up and obstacle avoidance.

Path discretization plays a critical role in the coordination of robot
movements, with three primary methods explored: equal length, optimal length,
and grid-based discretization. Each method has been evaluated for its
effectiveness in minimizing movement time for whole mission. The study
highlights the advantages of optimal length discretization for its ability to
handle complex environments by segmenting paths by maximizing the length of
non-collision segments and minimizing the number of transitions between
segments.

Collision and deadlock avoidance policies are addressed through advanced
algorithms and control mechanisms. The research employs Petri net models to
represent the discrete states and events associated with robot movements,
ensuring that robots operate without collisions and deadlocks. The modified
Banker’s algorithm is utilized for online deadlock avoidance, providing a
dynamic method to detect and resolve potential situations that unavoidably lead
to deadlocks as they occur. This approach enhances the flexibility and
robustness of the MMRS, allowing for safe and efficient navigation even in
unpredictable environments.

The experimental implementation of the proposed control framework was conducted
using TurtleBot 2 robots within the Robot Operating System 2 (ROS2) framework.
The system architecture integrates a central controller that manages robot
communication, mission assignments, and real-time decision-making. Each robot
operates as a node within the ROS network, employing the ROS Navigation 2 Stack
for path planning and execution. The experimental results demonstrate the
effectiveness of the proposed control strategies in various scenarios,
including collision and deadlock avoidance, highlighting the potential of the
system developed for practical applications.

In conclusion, this study contributes to the field of robotics by providing
innovative solutions for the supervisory control of MMRS.\@The proposed
hierarchical and hybrid control framework, along with advanced algorithms for
path discretization, collision avoidance, and deadlock prevention, offers a
robust approach to managing multi-robot systems in complex environments. Future
research could focus on further optimizing these algorithms and exploring their
applications in different robotic settings, enhancing the capabilities and
reliability of multi-robot systems for a wide range of tasks and missions.

\vspace*{1cm}
\noindent
\textbf{Availability of the project}

\noindent
The code and documentation for the project can be found at
\url{https://gitlab.com/jakub.kozlowicz/mmrs}. The repository has been
published under the MIT License, allowing for the free use and modification of
the software for research and educational purposes.

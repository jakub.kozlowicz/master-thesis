\chapter{Models and Algorithms of Supervisory Control}

The system considered in this thesis is a multi-mobile robot system (MMRS),
where multiple homogeneous robots are controlled by a central controller. The
robots have the same capabilities and share the same static environment. The
environment is a 2D motion space with obstacles and other robots. The robots
plan their paths independently based on the environment map and execute them
using their motion control algorithms (path following). The objective of the
central controller is to manage the movement of multiple robots, ensuring that
they can navigate safely without colliding with other robots or coming to a
situation where no robot can move forward
(deadlocks)~\cite{2023:Roszkowska:Janiec:DEDS,
  2021:Roszkowska:Jakubiak:ControlSynthesis}.

The system employs a hierarchical and hybrid control architecture (see
Fig.~\ref{fig:hierarhical-system-model}) to manage robot operations, ensuring
safe navigation and avoiding collisions and deadlocks. The control system is
structured into three distinct
levels~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis}:
\begin{enumerate}[noitemsep]
  \item \textbf{High-Level Supervisory Control:} This level supervises the
    general movement and coordination of robots across different path segments.
    It ensures that robots adhere to the mission plan and do not collide or
    enter into deadlock situations. The supervisory controller uses discrete
    event systems (DES) to model and manage the sequence of robot movements,
    ensuring efficient coordination and task execution.
  \item \textbf{Mid-Level Stage Traverse Control:} Acting as an intermediary,
    this level bridges the discrete supervisory control of the high level and
    the control of the low level continuous motion. Manages the transitions
    between discrete path segments and continuous motion, ensuring that robots
    move smoothly and effectively from one segment to another.
  \item \textbf{Low-Level Robot Motion Control:} This level is responsible for
    direct control of robot movements. It uses continuous-time systems (CTS) to
    manage the physical dynamics of each robot, ensuring accurate
    path-following and real-time response to changes in the environment. The
    robots execute their planned routes using motion control algorithms that
    adhere to kinematic and dynamic models.
\end{enumerate}
\begin{figure}[hptb]
  \centering
  \input{figures/hierarhical-system-model.tex}
  \caption{Hierarchical system
    model~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis}}%
  \label{fig:hierarhical-system-model}
\end{figure}

% ------------------------------------------------------------------------------
\section{Path Disctretization}

The task of path discretization is to divide the continuous path into discrete
segments that can be assigned to individual robots. This process is crucial for
coordinating the movement of multiple robots. Each robot can be assigned to one
segment at a time. This ensures that the robots can navigate safely without
colliding or entering deadlock situations. The discretization process is
required to transform the continuous movement of a robot into a discrete
process. The path discretization can be performed using various methods, such
as equal-length discretization, optimal length discretization, or grid-based
discretization. The choice of method depends on the specific requirements of
the system and the environment in which the robots operate.

\subsection{Equal Length Discretization}

Equal-length path discretization is the most straightforward method of path
discretization, in which the path is divided into segments of uniform length.
This method simplifies the assignment of segments to robots, as each segment
maintains a consistent length. However, this approach may not always be optimal
as it divides the path into equal parts regardless of intersections or
crossings with other paths. The algorithm iterates through the path,
cumulatively summing the distances between each pair of points and determining
whether a segment should be created. When the cumulative distance exceeds a
predefined length, a segment is created. This process is repeated until the
entire path has been segmented.
Figure~\ref{fig:multiple-robots-paths-equal-discretization} shows an example of
discretization of paths using the equal length method.
\begin{figure}[!hptb]
  \centering
  \input{figures/multi-robots-paths-equal.tex}
  \caption{Paths for multiple robots divided equally}%
  \label{fig:multiple-robots-paths-equal-discretization}
\end{figure}

\subsection{Optimal Length Discretization}

Optimal length path discretization represents a more advanced method that takes
into account proximity to other paths when segmenting the path. This approach
aims to maximize the lengths of collision-free segments while minimizing the
lengths of segments where collisions are possible. The safe distance is defined
as the sum of the robots' radiuses and an additional buffer distance to prevent
collisions. The algorithm iterates along the path, continuously assessing the
distance to neighboring paths and creating segments whenever this distance
falls below the predefined safe distance.
Figure~\ref{fig:multiple-robots-paths-optimal-discretization} shows an example
of the optimal length path discretization. In a situation where the paths do
not cross, the optimal length path discretization is equivalent to no
discretization.
\begin{figure}[!hptb]
  \centering
  \input{figures/multi-robots-paths-optimal.tex}
  \caption{Paths for multiple robots divided optimally}%
  \label{fig:multiple-robots-paths-optimal-discretization}
\end{figure}

\subsection{Grid Based Discretization}

Grid-based path discretization is based on a virtual grid map tailored to the
dimensions of the robot. Each cell in this grid is square, with side lengths
corresponding to the robot's diameter plus additional buffer. At the junctions
of these square cells, circular cells with a radius equal to the robot's radius
are placed, accommodating scenarios where the robot occupies four adjacent
cells. Additionally, smaller rectangular cells, with a width equivalent to the
robot's radius, are located next to the sides of the square cells. This
arrangement forms a comprehensive grid map that highlights subcells, indicating
whether a robot occupies a single cell, multiple cells, or overlaps between
cells. Path discretization follows this virtual grid, creating segments
whenever the path intersects these cells. For simplicity of the path division,
segments that indicate requirement of three and four adjacent cells are joined
together. This reduces the occurrences of short segments in which whole robot
will not be contained. The concept of grid-based path discretization is
depicted in Figure~\ref{fig:multiple-robots-paths-grid-discretization}. The
detailed part outlined by the circle is shown in
Figure~\ref{fig:multiple-robots-paths-grid-cropped}.
\begin{figure}[!hptb]
  \centering
  \input{figures/multi-robots-paths-grid.tex}
  \caption{Paths for multiple robots divided by grid}%
  \label{fig:multiple-robots-paths-grid-discretization}
\end{figure}
\begin{figure}[!hptb]
  \centering
  \input{figures/multi-robots-paths-grid-cropped.tex}
  \caption{Detail of paths for multiple robots divided by grid}%
  \label{fig:multiple-robots-paths-grid-cropped}
\end{figure}

% ------------------------------------------------------------------------------
\section{Supervisory Control Model}

The supervisory control model for a multi-mobile robot system (MMRS) presented
in this thesis leverages the Petri nets formalism to ensure efficient and
collision-free coordination among multiple robots. The model is designed to
manage the discrete events and states associated with robot movements, while
integrating with the continuous control of robot kinematics and dynamics.

Based on~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis}, the definition of
the Place/Transition (P/T) system is presented in
Definition~\ref{def:place-transition-system} along with the definition of the
state of the system and the firing rules for transitions in the system in
Definition~\ref{def:place-transition-system-state}. An enabled transition and
the marking of the net before and after its firing are shown in
Figure~\ref{fig:petri-nets-examples}.
\begin{definition}\label{def:place-transition-system}
  A place/transition system (P/T system) is a triple \( N = (P, T, F, M_0) \)
  such that:
  \begin{itemize}
    \item \( ( P \cup T, F) \) is a net where
      \begin{itemize}
        \item \( P \cup T\), \( P \cap T = \emptyset \), is the node set,
          that comprises two types of nodes: places \( p \in P \) and
          transitions \( t \in T \). The set \( T \) is further partitioned
          into two sets \( T_C \cup T_O \), \( T_C \cap T_O = \emptyset \),
          of controllable and observable (non-controllable) transitions.
        \item \( F \subseteq (P \times T) \cup (T \times P) \) is the flow
          relation, that constitutes the arc set of the net.
      \end{itemize}
    \item \( M_0 : P \rightarrow N \) is the initial state of the system,
      called the initial marking.
  \end{itemize}
\end{definition}
\begin{definition}\label{def:place-transition-system-state}
  The state of a P/T system is represented by marking \( M_0 : P \rightarrow N
  \), that is initially given by \( M_0 \) and then changes as a result of
  firing transitions according to the following rules:
  \begin{itemize}[noitemsep]
    \item only an enabled transition \( t \) can fire
    \item a transition \( t \) is enabled in marking \( M \) if and only if
      \( \forall p \in {}^\bullet t : M(p) \ge 1 \)
    \item if transition \( t \) fires in marking \( M \) then it causes the
      change of the marking to \( M_0 \) such that:
      \begin{equation*}
        M^{\prime}(p) = 
        \begin{cases}
          M(p) - 1 & \text{if } p \in\ ^\bullet t \backslash t^\bullet\\
          M(p) + 1 & \text{if } p \in\ t^\bullet \backslash {}^\bullet t\\
          M(p)     & \text{otherwise}
        \end{cases}
      \end{equation*}
      where:
      \begin{itemize}[noitemsep]
        \item \( {}^\bullet t = \{ p \in P : (p, t) \in F \} \) is the set of
          input places of transition \( t \),
        \item \( t^\bullet = \{ p \in P : (t, p) \in F \} \) is the set of
          output places of transition \( t \).
      \end{itemize}
  \end{itemize}
\end{definition}
\begin{figure}[hptb]
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \input{figures/petri-net-enabled-transition.tex}
    \caption{Petri net with enabled transition \( t \)}%
  \end{subfigure}%
  \hfill%
  \begin{subfigure}[t]{0.5\textwidth}
    \centering
    \input{figures/petri-net-follow-enabled-transition.tex}
    \caption{Marking of the Petri Net resulting from firing transition \( t \)}%
  \end{subfigure}
  \caption{Petri net transition}%
  \label{fig:petri-nets-examples}
\end{figure}

The supervisory controller benefits from the Petri nets formalism to model the
discrete events and states associated with robot movements based on the
discretization of the paths. The Petri net model for a single robot is shown in
Figure~\ref{fig:petri-net-single-robot}. This diagram illustrates a
\begin{figure}[hptb]
  \centering
  \input{figures/petri-net-model-single-robot.tex}
  \caption{Petri net model for path traversal of a single
    robot~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis}}%
  \label{fig:petri-net-single-robot}
\end{figure}
Place/Transition (P/T) system that models the movement of robot \( A_i \) along
its designated path \( p_i \), which is divided into \( n \) segments. The
visual representation includes various components:
\begin{itemize}[noitemsep]
  \item \textbf{Places}: Represented by circles (e.g.\ \( p_{i0}, q_{i1},
    p_{i1}, q_{i2}, \ldots \)), these indicate specific states or conditions of
    the robot. For example, \( p_{i0} \) and \( p_{i(n+1)} \) symbolize the
    position of the robot at the beginning and end of its path, respectively.
    The places \( q_{i1} \) to \( q_{i(n+1)} \) mark the robot's entry and exit
    points for each path segment. The places \( p_{ij} \), for \( j = 1,
    \ldots, n_i \), represent the travel of robot \( A_i \) along the different
    sectors of its path. Similarly, places \( q_{i,j} \), for \( j = 2, \ldots,
    n_i \), denote the transitions between consecutive sectors \( p_{ij} \) and
    \( p_{i(j+1)} \).
  \item \textbf{Transitions}: Depicted as black bars or short vertical lines
    (\( t_{ij}, u_{ij} \)), these represent the events or actions that change
    the robot's state.
    \begin{itemize}[noitemsep]
      \item Transitions \( t_{ij} \), for \( j = 1, \ldots, (n_i + 1) \) are
        controllable events representing permissions for the robot \( A_i \) to
        enter the segment \( p_{ij} \). For example \( t_{i1} \) and \(
        t_{i(n+1)} \) indicate when the robot is ready to move into the path
        and when it is ready to complete the path, respectively. Transitions \(
        t_{ij} \), for \( j = 2, \ldots, n_i \), occur when the robot is ready
        to move to the next sector \( p_{ij} \) upon completing the current
        sector \( p_{i(j-1)} \).
      \item Transitions \( u_{ij} \), for \( j = 1, \ldots n_i + 1 \) are
        observable events that occur when the robot fully enters or exits a
        specific path segment. For example, \( u_{i1} \) and \( u_{i(n+1)} \)
        indicate when the robot completely moves into the first segment or out
        of the path, respectively. Specifically, \( u_{ij} \), for \( j = 2,
        \ldots, n_i \), occur when the robot's disk entirely enters sector \(
        p_{ij} \), signifying no overlap with the previous sector \( p_{i(j-1)}
        \).
    \end{itemize}
  \item \textbf{Arcs}: The arrows connecting places to transitions and
    transitions to places show the flow of control or tokens. These arcs
    determine the direction of state changes within the system, illustrating
    how the robot moves from one state or place to another through the
    corresponding transitions. For example, an arc from \( p_{ij} \) to \(
    t_{ij} \) indicates the robot is ready to request permission to move to the
    next sector, while an arc from \( t_{ij} \) to \( q_{i(j+1)} \) indicates
    the robot's movement to the next sector upon receiving permission.
  \item \textbf{Marking}: The marking of the Petri net represents the current
    state of the system, indicating the places that contain tokens. The initial
    marking \( M_0 \) is defined by the initial position of the robot \( A_i \)
    at the beginning of its path. The marking changes as the robot moves along
    its path, transitioning between different sectors and segments. The token
    in a place \( p_{ij} \) signifies the robot's presence in that sector,
    while the token in a place \( q_{ij} \) indicates the robot's entry to the
    segment \( p_{ij} \) with its disk overlapping both \( p_{i(j-1)} \) and \(
    p_{ij} \) path segments.
\end{itemize}

The example paths for multiple robots are presented in
Figures~\ref{fig:multiple-robots-paths-equal-discretization},
and~\ref{fig:multiple-robots-paths-optimal-discretization},
and~\ref{fig:multiple-robots-paths-grid-discretization}.

The Petri net model for the system with multiple robots based on equal length
discretization is shown in Figure~\ref{fig:petri-net-multiple-robots-equal}.
The model is extended to include places and transitions for each robot \( A_i
\), where \( i = 1, \ldots, m \). A similar model can be used for the optimal
length discretization.

The Petri net model part for the details (see.
Fig.~\ref{fig:multiple-robots-paths-grid-cropped}) of the grid-based
discretization is shown in Figure~\ref{fig:petri-net-multiple-robots-grid}. The
model is only shown for a part because the full model would be too large to fit
on a single page. This is due to the fact that the grid-based discretization
typically creates more segments than the equal length discretization or optimal
length discretization. This can lead to more complex models and more complex
control algorithms. In addition, the size of the cells in the grid structure
can affect the efficiency of the control algorithm. If the cells are too large,
the robot may not be able to move along the path as planned. If the cells are
too small, the control algorithm may become too complex and require more
computational resources.

\begin{figure}[!hptb]
  \centering
  \input{figures/petri-net-model-multiple-robots-equal.tex}
  \caption{Petri net model for multiple robots based on equal length division}%
  \label{fig:petri-net-multiple-robots-equal}
\end{figure}
\begin{figure}[!hptb]
  \centering
  \input{figures/petri-net-model-multiple-robots-grid.tex}
  \caption{Petri net model for the detail of multiple robots based on grid
    division}%
  \label{fig:petri-net-multiple-robots-grid}
\end{figure}

% ------------------------------------------------------------------------------
\section{Collision Avoidance}

Collision avoidance is a critical aspect of robot motion control, ensuring that
robots can navigate safely in their environment without colliding with
obstacles or other robots. The system uses a simple collision avoidance
algorithm that restricts the robot movement based on the conflict relation
between the path segments determined through a direct path partition and
resulting from the motion area partitioned into cells, respectively.

\begin{definition}
  For a set of \( n \) robot paths, respectively partitioned into \( n_i \), \(
  i = 1, \ldots \), \( n \), sectors, two sectors are in conflict if they belong
  to two distinct paths and the minimal distance between these sectors is
  shorter than the diameter of the robot disk.
\end{definition}

\begin{definition}
  For a set of \( n \) robot paths, respectively partitioned into \( n_i \), \(
  i = 1, \ldots \), \( n \), sectors, two sectors are in conflict if they belong
  to two distinct paths and they pass at least one common cell in the grid
  structure.
\end{definition}

To model such restrictions, it is necessary to add additional places that will
represent the conflict segments of the paths.

The model in Figure~\ref{fig:petri-net-multiple-robots-equal} is extended to
include resources \( _{ij}r_{kl} \) modeled as places that represent the
resources required to enter the conflict segments of the paths. These resources
are necessary for robots to enter segments that collide with other paths. The
proposed model also includes the situation when two consecutive segments of the
same path are in conflict with the other path. This requires modeling the
occupied segments of the paths as two separate resources \( _{ij}r_{kl} \) and
\( _{ij}r_{k(l+1)} \) for the same path. The other robot can enter the segment
\( p_{ij} \) only if the segments \( p_{kl} \) or \( p_{k(l+1)} \) are not
occupied~\cite{2021:Roszkowska:Jakubiak:ControlSynthesis}.

The model presented in Figure~\ref{fig:petri-net-multiple-robots-grid} is
extended to include resources \( c_{xy} \) modeled as places that represent the
cells in the grid structure that are required to be free to enter the conflict
segments of the paths. The total number of resources \( c_{xy} \) is equal to
the number of cells in the grid structure.

% ------------------------------------------------------------------------------
\section{Deadlock Avoidance}

\begin{definition}
  Deadlock is any situation in which no member of some group of entities can
  proceed because each waits for another member, including itself, to take
  action.
\end{definition}

There are two types of policies that deal with deadlocks in the systems:
deadlock prevention and deadlock avoidance. The deadlock avoidance policy
allows for more flexibility by detecting potential deadlock situations while
the system is running and resolving them as they occur.

\begin{definition}
  In the considered P/T model of MMRS, marking \( M \) is ordered if there
  exists a permutation \( z_1, z_2, \ldots, z_n \) of the set \( N = \{ 1,2,
  \ldots, n \} \) such that for each pair \( (z_i, z_k) \in N \) the following
  is true. If \( z_i \ge z_k \) then \( M(p_{z_{k,j}}) = 0 \) and \(
  M(q_{z_{k,l}}) = 0 \), where \( p_{z_{k,l}} \) is any sector of robot \(
  A_{z_k} \) that is in conflict with any sector \( p_{z_{i,j}} \) of robot \(
  A_{z_i} \) that lies between its currently occupied sector (defined by \( M
  \)) and the nearest non-conflict sector.
\end{definition}
\begin{definition}
  For each transition \( t \) enabled in any marking \( M \), firing of \( t \)
  is safe if marking \( M^\prime{} \) resulting from \( M \xrightarrow{t}
  M^\prime{} \) is ordered.
\end{definition}

To ensure that the robots can reach their final states without getting stuck it
is necessary to reject the transitions \(M \xrightarrow{t} M^{\prime} \), such
that \( M^{\prime} \) is a deadlock state or unavoidably leads to a deadlock
state. The identification of such transitions is a NP-complete problem. To
avoid this complexity, the suboptimal deadlock avoidance policy is used. The
policy is based on the modified version of the Banker's
algorithm~\cite{2001:Dijkstra:ConcurrentProgrammingControl}, The modification
involves changes in the maximum demand of each process. Instead of the constant
maximal needs defined in the Banker's, the maximal needs depend on the marking
and are the sum of the resources required for the current operation plus all
the operations succeeding this one up to the nearest private resource. This
modification reduces the restrictions on the resource requirements and allows
more flexibility in the allocation of resources. The deadlock avoidance
procedure takes the form of Algorithm~\ref{alg:bankers-algorithm}, given below.
The variables in the algorithm are defined as follows:
\begin{itemize}[noitemsep]
  \item \( \text{Available} \) is a vector of length (\( m \)) that represents
    the number of available resources of each type.
  \item \( \text{Max}_i \) is a matrix of size (\( n \times m \)) that
    represents the maximum demand of each process (\( n \)) for each resource
    type (\( m \)). The maximal needs are the sum of the resources required for
    the current operation plus all the operations succeeding this one up to the
    nearest private resource.
  \item \( \text{Allocation}_i \) is a matrix of size (\( n \times m \)) that
    represents the number of resources of each type (\( m \)) currently
    allocated to each process (\( n \)).
  \item \( \text{Finish}_i \) is an vector of size \( n \) that indicates
    whether each process (\( n \)) has completed its task.
  \item \( \text{Work} \) is a vector of length \( m \) that represents the
    number of resources available of each type after the completion of all the
    processes for which \( \text{Finish}_i = true \).
\end{itemize}
\begin{algorithm}[hptb]
  \caption{Modified Banker's algorithm for deadlock avoidance}%
  \label{alg:bankers-algorithm}
  \begin{algorithmic}[1]
    \State{} \( \text{Work} \gets \text{Available} \)
    \State{} \( \text{Finish}[i] \gets \text{false} \) for \( i = 1, 2, \ldots, n \)
    \While{true}
    \State{} \( \text{Safe} \gets \text{false} \)
    \For{\( i = 1 \) to \( n \)}
    \If{\( \text{Finish}[i] = \text{false} \) \textbf{and} \( \text{Max}[i] - \text{Allocation}[i] \leq \text{Work} \)}
    \State{} \( \text{Work} \gets \text{Work} + \text{Allocation}[i] \)
    \State{} \( \text{Finish}[i] \gets \text{true} \)
    \State{} \( \text{Safe} \gets \text{true} \)
    \EndIf{}
    \EndFor{}
    \If{\( \text{Safe} = \text{false} \)}
    \State{} \textbf{break}
    \EndIf{}
    \EndWhile{}
    \If{\( \text{Safe} = \text{true} \)}
    \State{ \Return{ true } }
    \Else{}
    \State{ \Return{ false } }
    \EndIf{}
  \end{algorithmic}
\end{algorithm}

% ------------------------------------------------------------------------------
\section{Robot Model}

The mobile robots are modeled as a differential drive robot, driven by two
independently powered wheels on either side. The kinematic model of the robot
describes its motion based on the velocities of the wheel, while the dynamic
model considers the forces and torques acting on the robot.

The kinematic model of the mobile robots is defined by the following
equations~\cite{2011:Siegwart:IntroductionAutonomousMobileRobots} (see
Fig.~\ref{fig:differential-drive-robot-model}):
\begin{equation}
  \begin{cases}
    \dot{x} = v \cos(\theta) \\
    \dot{y} = v \sin(\theta) \\
    \dot{\theta} = \dfrac{v_r - v_l}{D}
  \end{cases}
\end{equation}
where:
\begin{wrapfigure}{r}{0.49\textwidth}
  \centering
  \input{figures/diff-robot-model.tex}
  \caption{Differential drive robot model}%
  \label{fig:differential-drive-robot-model}
\end{wrapfigure}
\begin{itemize}[noitemsep]
  \item \( x \) and \( y \) represent the robot’s coordinates in the global
    frame,
  \item \( v \) is the linear velocity,
  \item \( \theta \) is the heading angle,
  \item \( D \) is the distance between the wheels,
  \item \( v_r \) and \( v_l \) are the linear velocities of the right and left
    wheels, respectively.
\end{itemize}

The kinematic model describes the motion of the robot in terms of its linear
velocities. The low-level robot motion control requires this to generate the
appropriate wheel velocities to move the robot along the planned path.

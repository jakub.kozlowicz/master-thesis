\cleardoublepage{}
\phantomsection{}
\addcontentsline{toc}{chapter}{Introduction}
\chapter*{Introduction}

\epigraph{\textit{The significant problems we face cannot be solved at the same
level of thinking we were at when we created them.}}{Albert Einstein}

Since the early days of human history, our quest to explore, understand, and
replicate the wonders of nature has driven us forward. This relentless
curiosity, which has spanned centuries and continents, has been the foundation
of monumental discoveries in science and technology. As we transitioned from
studying the stars to launching spacecraft, from crafting simple tools to
developing complex machines, our progress has been marked by a continual
synthesis of knowledge across disciplines. One of the most profound areas of
modern exploration is robotics, where the convergence of engineering,
artificial intelligence, and control systems is creating unprecedented
capabilities~\cite{siciliano2016springer, dudek2010computational,
  corke2017robotics}.

Robotics, a field once confined to the realms of science fiction, has now
become a vital area of research and development~\cite{siciliano2016springer,
  arkin1998behavior}. This transformation is reflected in the diversity and
complexity of tasks that robots perform today, ranging from surgical procedures
and space exploration to disaster response and industrial automation. The term
``robot,'' has evolved significantly from its literary origins to become
synonymous with autonomous and intelligent systems that interact with the
physical world~\cite{thrun2005probabilistic, parker2008distributed}.

Central to the advancement of robotics is the concept of control systems, which
are fundamental to the operation and coordination of robotic entities. Control
systems enable robots to perform precise movements, maintain stability, and
adapt to dynamic environments~\cite{corke2017robotics, yim2019modular}. In
particular, the supervisory control of multiple robots, which involves the
supervision and coordination of the activities of several robots
simultaneously, presents a unique set of challenges and
opportunities~\cite{lavalle2006planning, fukuda1987dynamically}. This involves
intricate processes such as task allocation, path planning, and collision
avoidance, all of which must be managed to ensure the effective and safe
operation of robots in various settings~\cite{jennings1998agent,
  alami1998architecture}.

The focus of this thesis is on the development of models and algorithms for the
supervisory control of multiple mobile robots~\cite{kalra2005hoplite}. These
robots are tasked with navigating complex environments, which may include
obstacles, dynamic elements, and other robots. The objective is to create a
control framework that can handle these complexities, ensuring that robots
operate efficiently and effectively while avoiding collisions and
deadlocks~\cite{brooks1991intelligence}. This involves integrating discrete and
continuous control systems to manage high-level coordination and low-level
execution of robotic tasks~\cite{bellingham2007robotics, moore2010supervisory}.

\section*{Scope of the Thesis}

The purpose of this thesis is to explore and implement selected models and
algorithms underlying the supervisory control of Multiple Mobile Robot Systems
(MMRS). The project will investigate the impact of specific control solutions on
the behavior of MMRS, providing valuable information on their efficiency and
reliability in different operational contexts. The aim is to develop algorithms
that facilitate the coordination and control of robots, allowing them to
perform tasks in a collaborative and efficient manner. Research focuses on
creating a hierarchical control architecture that integrates discrete event
systems (DES) and continuous-time systems (CTS), leveraging the strengths of
both approaches to achieve optimal control and coordination. The study will
examine how various control strategies influence the behavior of multiple
robots, focusing on improving their coordination, task allocation, and
collision avoidance capabilities.

The \textbf{research aspect} involves a comprehensive survey and comparative
analysis of different approaches to the discrete representation of concurrent
robot motion processes. This includes evaluating control solutions that ensure
the correct and efficient operation of MMRS.\@The study will analyze various
control architectures and algorithms to identify their strengths and
limitations in handling the complexities of multirobot coordination and
control.

The \textbf{engineering aspect} of this thesis focuses on the development of a
software framework for the MMRS supervisor and mobile robots navigation. This
includes designing and implementing the supervisor code, followed by a series
of experiments to evaluate the behavior of the MMRS under different control
models and algorithms. The experiments aim to evaluate the performance and
reliability of the proposed solutions in real-world scenarios, providing
empirical data to support the theoretical findings.

By addressing these aspects, this thesis aims to contribute to the field of
robotics by providing innovative solutions for MMRS supervisory control. The
research findings and developed software tools will enhance the capabilities
and reliability of multi-robot systems, enabling them to perform complex tasks
in dynamic and unpredictable environments.


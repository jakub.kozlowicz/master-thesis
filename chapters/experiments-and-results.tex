\chapter{Experiments and Results}

This chapter delineates the findings from a series of experiments aimed at
assessing the efficacy of a novel supervisory control strategy for multi-robot
systems. The experiments were orchestrated in a static environment where the
presence was limited to robots. The experimental setups involved configurations
of two, three, and four robots, each receiving dynamic mission assignments. The
results were meticulously evaluated across various configurations, focusing on
collision scenarios and comparing the performance indicators for each
experimental arrangement.

\section{Detailed Description of the Experiments}

\subsection{Configuration of the Experiments}

The experiments were designed methodically to determine the effectiveness of
the supervisory control system within a controlled environment. In this static
environment, obstacles were placed along the peripheries and robots were tasked
with navigating through predefined waypoints. Three distinct configurations
were employed:
\begin{enumerate}[noitemsep]
  \item \textbf{Two robots:} This initial setup was intended to evaluate the
    basic operational capabilities of the system in managing multiple robots
    simultaneously.
  \item \textbf{Three robots:} Introducing an additional robot increased
    complexity, necessitating improved algorithms for collision avoidance and
    sophisticated path planning.
  \item \textbf{Four robots:} This configuration tested the scalability and
    robustness of the system to coordinate multiple robotic units.
\end{enumerate}

The segmentation length for equal path division was established at 1.2 meters,
while a grid size of 0.8 meters was utilized for the grid division method.

\subsection{Robot Paths}

The trajectories executed by the robots in different configurations are shown
in Figure~\ref{fig:expriments-robots-paths}. Each subfigure visually represents
the paths for varying numbers of robots, illustrating scenarios both with and
without collisions.
\begin{figure}[hptb]
  \centering
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{figures/experiments/2-robots-no-collision.tex}
    \caption{Paths of 2 robots with no collisions}%
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{figures/experiments/2-robots-collision.tex}
    \caption{Paths of 2 robots with collisions}%
  \end{subfigure}
  \vfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{figures/experiments/3-robots-no-collision.tex}
    \caption{Paths of 3 robots with no collisions}%
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{figures/experiments/3-robots-collision.tex}
    \caption{Paths of 3 robots with collisions}%
  \end{subfigure}
  \vfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{figures/experiments/4-robots-no-collision.tex}
    \caption{Paths of 4 robots with no collisions}%
  \end{subfigure}
  \hfill
  \begin{subfigure}[b]{0.45\textwidth}
    \centering
    \input{figures/experiments/4-robots-collision.tex}
    \caption{Paths of 4 robots with collisions}%
  \end{subfigure}
  \caption{Paths of robots used in experiments}%
  \label{fig:expriments-robots-paths}
\end{figure}

\section{Presentation of Results}

The results of the experiments are systematically presented in tabular formats,
detailing performance metrics such as mission duration, planning time,
segmentation time, the total number of segments, and the aggregate length of
paths. Each table contrasts the results of different path division strategies:
equal, optimal, and grid.

\subsection{Outcomes for Two Robots in Non-collision Scenarios}

The results, as shown in Table~\ref{tab:2-robots-no-collisions}, indicate that
the optimal division method consistently yielded the shortest mission durations
compared to the equal and grid methods. This suggests that the optimal method
can effectively minimize the complexity of movement, thereby facilitating
faster completion of missions. The absence of collisions allowed the optimal
division to uniformly divide the path into one segment, ensuring uninterrupted
movement for the robots. In contrast, the grid method, despite its structured
approach, resulted in the longest mission durations due to an increased number
of segments. In particular, in scenarios involving short paths without
collisions, all methods demonstrated similar segmentation times.
\begin{table}[!hptb]
  \centering
  \caption{Results of the experiments with 2 robots and no collisions}%
  \label{tab:2-robots-no-collisions}
  \begin{tabular}{ccccccc}
    \toprule
    \textbf{division type} & \textbf{robot} & \parbox[c]{2cm}{\centering \textbf{mission}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{planning}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{division}\\\textbf{time [s]}} & \textbf{segments [\#]} & \textbf{length [m]} \\ \midrule
    \multirow{2}{*}{\centering equal}   & 1 & 22.09 & 0.04 & 0.02 & 4  & 4.25 \\ \cmidrule(lr){2-7}
                                        & 2 & 19.07 & 0.04 & 0.02 & 4  & 4.28 \\ \midrule
    \multirow{2}{*}{\centering optimal} & 1 & 22.03 & 0.05 & 0.03 & 1  & 4.22 \\ \cmidrule(lr){2-7}
                                        & 2 & 20.13 & 0.03 & 0.03 & 1  & 4.25 \\ \midrule
    \multirow{2}{*}{\centering grid}    & 1 & 26.27 & 0.04 & 0.03 & 22 & 4.23 \\ \cmidrule(lr){2-7}
                                        & 2 & 22.07 & 0.04 & 0.03 & 20 & 4.28 \\ \bottomrule
  \end{tabular}
\end{table}

\subsection{Outcomes for Three Robots in Non-collision Scenarios}

In scenarios involving three robots, the optimal division method once again
proved superior by demonstrating the lowest mission times, underscoring its
efficacy in dynamic mission allocation and execution. The equal division method
exhibited moderate performance, while the grid method, due to its extensive
segmentation, incurred longer mission times. The results, detailed in
Table~\ref{tab:3-robots-no-collisions}, show an increase in the division times
for the optimal method, attributed to the increased complexity introduced by
the additional robot. The other methods maintained division times comparable to
those of previous setups.
\begin{table}[!hptb]
  \centering
  \caption{Results of the experiments with 3 robots and no collisions}%
  \label{tab:3-robots-no-collisions}
  \begin{tabular}{ccccccc}
    \toprule
    \textbf{division type} & \textbf{robot} & \parbox[c]{2cm}{\centering \textbf{mission}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{planning}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{division}\\\textbf{time [s]}} & \textbf{segments [\#]} & \textbf{length [m]} \\ \midrule
            & 1 & 27.07 & 0.06 & 0.03 & 4  & 4.28 \\ \cmidrule(lr){2-7}
    equal   & 2 & 24.87 & 0.09 & 0.03 & 4  & 4.40 \\ \cmidrule(lr){2-7}
            & 3 & 37.16 & 0.10 & 0.03 & 5  & 5.55 \\ \midrule
            & 1 & 25.35 & 0.06 & 0.08 & 1  & 4.22 \\ \cmidrule(lr){2-7}
    optimal & 2 & 21.62 & 0.06 & 0.08 & 1  & 4.37 \\ \cmidrule(lr){2-7}
            & 3 & 29.77 & 0.05 & 0.08 & 1  & 5.51 \\ \midrule
            & 1 & 37.54 & 0.06 & 0.04 & 22 & 4.25 \\ \cmidrule(lr){2-7}
    grid    & 2 & 35.80 & 0.04 & 0.04 & 23 & 4.40 \\ \cmidrule(lr){2-7}
            & 3 & 48.26 & 0.04 & 0.04 & 27 & 5.51 \\ \bottomrule
    \end{tabular}
\end{table}

\subsection{Outcomes for Four Robots in Non-collision Scenarios}

Similar trends were observed with four robots, where the optimal division
method consistently led to the most efficient outcomes in terms of the duration
of the mission. The results, encapsulated in
Table~\ref{tab:4-robots-no-collisions}, reveal that while the equal division
method provided moderate results, the grid method was the least efficient,
requiring the highest mission times due to a larger number of segments. The
division times for the optimal method remained relatively stable, whereas those
for the other methods were comparable to those in simpler scenarios.
\begin{table}[!hptb]
  \centering
  \caption{Results of the experiments with 4 robots and no collisions}%
  \label{tab:4-robots-no-collisions}
  \begin{tabular}{ccccccc}
    \toprule
    \textbf{division type} & \textbf{robot} & \parbox[c]{2cm}{\centering \textbf{mission}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{planning}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{division}\\\textbf{time [s]}} & \textbf{segments [\#]} & \textbf{length [m]} \\ \midrule
    \multirow{5}{*}{\centering equal}   & 1 & 31.32 & 0.12 & 0.02 & 3  & 3.26 \\ \cmidrule(lr){2-7}
                                        & 2 & 24.70 & 0.06 & 0.02 & 3  & 3.24 \\ \cmidrule(lr){2-7}
                                        & 3 & 26.23 & 0.07 & 0.02 & 3  & 3.32 \\ \cmidrule(lr){2-7}
                                        & 4 & 37.37 & 0.05 & 0.02 & 3  & 3.25 \\ \midrule
    \multirow{5}{*}{\centering optimal} & 1 & 22.33 & 0.04 & 0.05 & 1  & 2.82 \\ \cmidrule(lr){2-7}
                                        & 2 & 16.78 & 0.06 & 0.05 & 1  & 2.82 \\ \cmidrule(lr){2-7}
                                        & 3 & 18.63 & 0.05 & 0.05 & 1  & 2.82 \\ \cmidrule(lr){2-7}
                                        & 4 & 21.63 & 0.04 & 0.05 & 1  & 2.85 \\ \midrule
    \multirow{5}{*}{\centering grid}    & 1 & 26.16 & 0.04 & 0.03 & 16 & 3.27 \\ \cmidrule(lr){2-7}
                                        & 2 & 28.47 & 0.05 & 0.03 & 15 & 3.25 \\ \cmidrule(lr){2-7}
                                        & 3 & 37.64 & 0.05 & 0.03 & 22 & 3.33 \\ \cmidrule(lr){2-7}
                                        & 4 & 27.16 & 0.06 & 0.03 & 18 & 3.25 \\ \bottomrule
    \end{tabular}
\end{table}

\subsection{Outcomes for Two Robots in Collision Scenarios}

In collision scenarios involving two robots, the results from
Table~\ref{tab:2-robots-collisions} demonstrate a marked impact of collisions
on mission times. Notably, the optimal division method again exhibited the
shortest mission times, highlighting its capability to effectively manage
collisions. Although the equal division method aligned the number of segments
with the optimal method, it resulted in longer mission durations due to the
higher number of segments that collide with each other, thereby necessitating
additional waiting times for collision avoidance. In contrast, the grid method,
despite its structured segmentation, incurred the longest mission times owing
to the larger number of segments. The introduction of collisions notably
increased the division times for the optimal method, while the division times
for the other methods remained consistent with non-collision scenarios.
\begin{table}[!hptb]
  \centering
  \caption{Results of the experiments with 2 robots and collisions}%
  \label{tab:2-robots-collisions}
  \begin{tabular}{ccccccc}
    \toprule
    \textbf{division type} & \textbf{robot} & \parbox[c]{2cm}{\centering \textbf{mission}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{planning}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{division}\\\textbf{time [s]}} & \textbf{segments [\#]} & \textbf{length [m]} \\ \midrule
    \multirow{2}{*}{\centering equal}   & 1 & 54.89 & 0.04 & 0.03 & 5  & 5.43 \\ \cmidrule(lr){2-7}
                                        & 2 & 34.08 & 0.06 & 0.03 & 5  & 5.33 \\ \midrule
    \multirow{2}{*}{\centering optimal} & 1 & 38.73 & 0.04 & 0.06 & 5  & 5.38 \\ \cmidrule(lr){2-7}
                                        & 2 & 34.48 & 0.06 & 0.06 & 5  & 5.36 \\ \midrule
    \multirow{2}{*}{\centering grid}    & 1 & 57.97 & 0.04 & 0.04 & 30 & 5.43 \\ \cmidrule(lr){2-7}
                                        & 2 & 49.80 & 0.06 & 0.04 & 25 & 5.33 \\ \bottomrule
    \end{tabular}
\end{table}

\subsection{Outcomes for Three Robots in Collision Scenarios}

The dynamics of three robots experiencing collisions present varied results, as
detailed in Table~\ref{tab:3-robots-collisions}. Contrary to non-collision
scenarios, the optimal method did not lead but showed moderate mission times,
whereas the equal division method exhibited the best performance due to
effective path placements. The optimal method, despite having the fewest
segments, faced challenges as robots had to wait for clearance due to longer
collision segments compared to other scenarios, thus affecting mission
durations. The grid method, consistently producing the longest mission times
and highest segment count, also demonstrated inefficiencies in the effective
handling of collisions. Division times in the optimal method were significantly
higher, reflecting the complexity added by collisions, while the other methods
showed division times similar to their non-collision counterparts.
\begin{table}[!hptb]
  \centering
  \caption{Results of the experiments with 3 robots and collisions}%
  \label{tab:3-robots-collisions}
  \begin{tabular}{ccccccc}
    \toprule
    \textbf{division type} & \textbf{robot} & \parbox[c]{2cm}{\centering \textbf{mission}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{planning}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{division}\\\textbf{time [s]}} & \textbf{segments [\#]} & \textbf{length [m]} \\ \midrule
            & 1 & 26.17 & 0.08 & 0.03 & 4  & 4.54 \\ \cmidrule(lr){2-7}
    equal   & 2 & 51.87 & 0.05 & 0.03 & 5  & 6.01 \\ \cmidrule(lr){2-7}
            & 3 & 36.35 & 0.07 & 0.03 & 6  & 6.26 \\ \midrule
            & 1 & 67.06 & 0.06 & 0.11 & 3  & 4.55 \\ \cmidrule(lr){2-7}
    optimal & 2 & 49.63 & 0.10 & 0.11 & 3  & 5.98 \\ \cmidrule(lr){2-7}
            & 3 & 40.40 & 0.09 & 0.11 & 3  & 6.24 \\ \midrule
            & 1 & 45.69 & 0.04 & 0.04 & 21 & 4.58 \\ \cmidrule(lr){2-7}
    grid    & 2 & 72.22 & 0.06 & 0.04 & 36 & 6.04 \\ \cmidrule(lr){2-7}
            & 1 & 49.70 & 0.05 & 0.04 & 37 & 6.26 \\ \bottomrule
    \end{tabular}
\end{table}

\subsection{Outcomes for Four Robots in Collision Scenarios}

The scenario involving four robots with collisions, represented in
Table~\ref{tab:4-robots-collisions}, is indicative of the complexity and
challenges posed by multiple robots operating in confined spaces with potential
conflicts. The optimal division method, though not the most efficient in
segment number, resulted in better performance in terms of mission time
compared to equal division. This suggests that while the optimal method managed
the paths more effectively during collisions, the equal division method did not
adequately account for proximity and potential path interferences, resulting in
more frequent collisions and, consequently, longer mission times. The grid
method once again resulted in the most prolonged mission times and highest
segment counts, underscoring its lack of efficiency in collision-rich
environments. The division times for the optimal method peaked due to the
intricate calculations required to manage the increased robot count and
collision possibilities, whereas the other methods maintained consistent
division times as seen in scenarios without collisions.
\begin{table}[!hptb]
  \centering
  \caption{Results of the experiments with 4 robots and collisions}%
  \label{tab:4-robots-collisions}
  \begin{tabular}{ccccccc}
    \toprule
    \textbf{division type} & \textbf{robot} & \parbox[c]{2cm}{\centering \textbf{mission}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{planning}\\\textbf{time [s]}} & \parbox[c]{2cm}{\centering \textbf{division}\\\textbf{time [s]}} & \textbf{segments [\#]} & \textbf{length [m]} \\ \midrule
    \multirow{5}{*}{\centering equal}   & 1 & 65.24 & 0.04 & 0.03 & 4  & 4.56 \\ \cmidrule(lr){2-7}
                                        & 2 & 32.06 & 0.07 & 0.03 & 4  & 4.67 \\ \cmidrule(lr){2-7}
                                        & 3 & 31.85 & 0.09 & 0.03 & 4  & 4.60 \\ \cmidrule(lr){2-7}
                                        & 4 & 31.72 & 0.06 & 0.03 & 4  & 4.59 \\ \midrule
    \multirow{5}{*}{\centering optimal} & 1 & 36.73 & 0.04 & 0.12 & 5  & 4.60 \\ \cmidrule(lr){2-7}
                                        & 2 & 28.32 & 0.05 & 0.12 & 5  & 4.65 \\ \cmidrule(lr){2-7}
                                        & 3 & 28.69 & 0.07 & 0.12 & 5  & 4.57 \\ \cmidrule(lr){2-7}
                                        & 4 & 32.86 & 0.05 & 0.12 & 5  & 4.57 \\ \midrule
    \multirow{5}{*}{\centering grid}    & 1 & 49.05 & 0.06 & 0.04 & 23 & 4.62 \\ \cmidrule(lr){2-7}
                                        & 2 & 67.31 & 0.26 & 0.04 & 21 & 4.67 \\ \cmidrule(lr){2-7}
                                        & 3 & 58.95 & 0.04 & 0.04 & 24 & 4.61 \\ \cmidrule(lr){2-7}
                                        & 4 & 42.80 & 0.04 & 0.04 & 25 & 4.60 \\ \bottomrule
    \end{tabular}
\end{table}

\section{Analysis and Discussion}

The experimental investigations elucidated the distinct capabilities and
limitations inherent to each path division strategy:
\begin{itemize}
  \item \textbf{Equal Division Method:} Characterized by its simplicity, this
    approach performs competently in scenarios with minimal complexity and no
    collisions. It encounters difficulties in more complex environments that
    include multiple robots and potential collisions, resulting in extended
    mission durations. The calculation times remain the lowest and consistent
    across all tests due to the straightforward nature of the computations.
  \item \textbf{Optimal Division Method:} This strategy is particularly
    effective at minimizing mission times in environments devoid of collisions
    through adept path segmentation. Despite a decrease in performance when
    collisions occur, it still maintains superiority over other methods. The
    computation times are elevated, reflecting the intricacies of the
    segmentation process. As the scenarios become more complex with more
    robots, the time required for path division increases. In particular, this
    method consistently uses the fewest segments among the tested approaches.
  \item \textbf{Grid Division Method:} This method generally leads to the
    longest mission durations because of its extensive segmentation, which also
    introduces added complexity to the overall model. Although the division
    times are comparable to those of the equal division method, the number of
    segments required far exceeds those of the other strategies.
\end{itemize}

In conclusion, the choice of path division strategy should depend on the
specific demands of the mission and the operational dynamics of the
environment. The optimal division method is preferable in complex scenarios
where efficiency is paramount, whereas the equal division method suffices for
less complicated situations. The findings from these experiments are pivotal in
guiding the selection of an appropriate strategy, ensuring both efficiency and
adaptability in the supervisory control of multi-robot systems. All of the
methods provide a solution in which robots do not collide with each other. The
addition of the deadlock avoidance mechanism ensures that robots do not get
stuck in a deadlock situation. All robots in all configurations were able to
reach their destinations without any deadlock situations.

